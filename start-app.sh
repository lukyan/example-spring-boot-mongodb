#!/usr/bin/env bash
export JAVA_HOME=/opt/jdks/openjdk/x64/jdk-11.0.2
export PATH=$JAVA_HOME/bin:$PATH
java -jar target/example-spring-boot-mongodb-0.1.0-SNAPSHOT.jar