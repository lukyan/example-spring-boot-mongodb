/*
package com.lukyan.example.jep;

import jep.Jep;
import jep.JepException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
public class JepTest {

    @TestConfiguration
    static class JepTestConfig {

        @Bean
        public Jep jep() throws JepException {
            return new Jep();
        }
    }

    @Autowired
    private Jep jep;

    @Test
    public void test0001Jep() throws JepException {
        jep.eval("from java.lang import System");
        jep.eval("s = 'Hello World'");
        jep.eval("System.out.println(s)");
        jep.eval("print(s)");
        jep.eval("x = s[1:-1]");
        Object x = jep.getValue("x");
        assertTrue("ello Worl".equals(x));
    }
}
*/
