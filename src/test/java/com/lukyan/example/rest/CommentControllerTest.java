package com.lukyan.example.rest;

import com.lukyan.example.Application;
import com.lukyan.example.constant.Admins;
import com.lukyan.example.constant.Users;
import com.lukyan.example.dto.CommentThreadDto;
import com.lukyan.example.mongo.entity.Comment;
import com.lukyan.example.mongo.entity.CommentSourceType;
import com.lukyan.example.mongo.entity.CommentThread;
import com.lukyan.example.mongo.entity.ThreadSourceType;
import com.lukyan.example.service.api.CommentService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"server.port=18087"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommentControllerTest {

    private static Logger log = LoggerFactory.getLogger(CommentControllerTest.class);

    private final static String URL = "http://localhost:18087/api";

    @Autowired
    @Qualifier("testRestTemplate")
    private TestRestTemplate restTemplate;

    @Autowired
    private CommentService commentService;

    private final static String TEXT_ADDED = "textAdded";
    private final static String TEXT_BEFORE = "textBefore";
    private final static String TEXT_AFTER = "textAfter";

    private CommentThread thread;
    private Comment comment;

    @Before
    public void setUp(){
        this.thread = createThread();
        this.comment = createComment(this.thread, Users.USER1, TEXT_BEFORE);
    }

    HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    private HttpHeaders headersAdmin1() {
        return createHeaders(Admins.ADMIN1, "password");
    }

    private HttpHeaders headersUser1() {
        return createHeaders(Users.USER1, "password");
    }

    private CommentThread createThread() {
        return commentService.createThread(ThreadSourceType.NONE, ThreadSourceType.NONE.name());
    }

    private Comment createComment(CommentThread thread, String author, String text) {
        return commentService.addComment(CommentSourceType.THREAD, thread.getId(), author, text);
    }

    @Test
    public void test001CreateThread() {
        HttpHeaders headers = headersAdmin1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        String url = String.format("%s/thread/add/%s/%s", URL, ThreadSourceType.NONE, UUID.randomUUID().toString());
        ResponseEntity<CommentThread> result =
                restTemplate.exchange(url, HttpMethod.POST, entity, CommentThread.class);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void test002GetThread() {
        HttpHeaders headers = headersUser1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        CommentThread commentThreadEntity = createThread();
        String url = String.format("%s/thread/get/%s/%s/%s", URL, commentThreadEntity.getSourceType(),
                commentThreadEntity.getSourceId(), commentThreadEntity.getId());
        ResponseEntity<CommentThreadDto> result =
                restTemplate.exchange(url, HttpMethod.GET, entity, CommentThreadDto.class);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void test003AddComment() {
        HttpHeaders headers = headersUser1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        CommentThread commentThreadEntity = createThread();
        String url = String.format("%s/comment/add/%s/%s", URL,
                CommentSourceType.THREAD, commentThreadEntity.getId());
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("text", TEXT_ADDED);
        ResponseEntity<Comment> result =
                restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, entity, Comment.class);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void test004UpdateComment() {
        HttpHeaders headers = headersUser1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        String url = String.format("%s/comment/update/%s/%s/%s", URL,
                comment.getSourceType(), comment.getSourceId(), comment.getId());
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("text", TEXT_AFTER);
        ResponseEntity<Void> result =
                restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.PUT, entity, Void.class);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void test005DeleteComment() {
        HttpHeaders headers = headersUser1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        String url = String.format("%s/comment/delete/%s/%s/%s", URL,
                comment.getSourceType(), comment.getSourceId(), comment.getId());
        ResponseEntity<Void> result =
                restTemplate.exchange(url, HttpMethod.DELETE, entity, Void.class);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void test006DeleteAllComments() {
        HttpHeaders headers = headersAdmin1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        String url = String.format("%s/comment/delete/all", URL,
                comment.getSourceType(), comment.getSourceId(), comment.getId());
        ResponseEntity<Void> result =
                restTemplate.exchange(url, HttpMethod.DELETE, entity, Void.class);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void test007GetAllComments() {
        HttpHeaders headers = headersUser1();
        HttpEntity<Map> entity = new HttpEntity(headers);
        String url = String.format("%s/comment/all", URL);
        ResponseEntity<List> result =
                restTemplate.exchange(url, HttpMethod.GET, entity, List.class);
        assertEquals(200, result.getStatusCodeValue());
    }

}
