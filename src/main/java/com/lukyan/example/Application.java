package com.lukyan.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.lukyan.example"})
public class Application {

    private final static Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
//        ApplicationContext ctx= SpringApplication.run(Application.class, args);
        SpringApplication.run(Application.class, args);
        var class12 = new Class12();
        String hello = class12.hello();
        log.debug(hello);
/*
        boolean isHello = switch (hello){
            case "Hello, World!!!" -> true;
            default -> false;
        };
        System.out.println(isHello);
*/
/*
        System.out.println("Let's inspect the beans provided by Spring Boot:");
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
*/
    }
}
