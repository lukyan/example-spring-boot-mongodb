package com.lukyan.example.rest;

import com.lukyan.example.dto.CommentThreadDto;
import com.lukyan.example.mongo.entity.Comment;
import com.lukyan.example.mongo.entity.CommentSourceType;
import com.lukyan.example.mongo.entity.CommentThread;
import com.lukyan.example.mongo.entity.ThreadSourceType;
import com.lukyan.example.service.api.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@Api(value = "comments", description = "Operations for manipulating threads/comments")
public class CommentController extends CommonController {

    private final CommentService service;

    @Autowired
    public CommentController(CommentService service) {
        this.service = service;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(path = "thread/add/{sourceType}/{sourceId}")
    @ApiOperation(value = "Create comment thread", response = CommentThread.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully create thread"),
            @ApiResponse(code = 201, message = "FIXME"),
            @ApiResponse(code = 401, message = "You are not authorized to create thread"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity<CommentThread> createThread(@PathVariable("sourceType") ThreadSourceType sourceType,
                                                      @PathVariable("sourceId") String sourceId) {
        return new ResponseEntity<>(service.createThread(sourceType, sourceId), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "thread/get/{sourceType}/{sourceId}/{id}")
    public ResponseEntity<CommentThreadDto> getThread(@PathVariable("sourceType") ThreadSourceType sourceType,
                                                      @PathVariable("sourceId") String sourceId,
                                                      @PathVariable("id") String id) {
        return new ResponseEntity<>(service.getThread(sourceType, sourceId, id), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(path = "comment/add/{sourceType}/{sourceId}")
    public ResponseEntity<Comment> addComment(@PathVariable("sourceType") CommentSourceType sourceType,
                                              @PathVariable("sourceId") String sourceId,
                                              @RequestParam(name = "text") String text) {
        var author = getCurrentUser();
        return new ResponseEntity<>(service.addComment(sourceType, sourceId, author, text), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "comment/all")
    public ResponseEntity<List<Comment>> getAllComments() {
        return new ResponseEntity<>(service.getAllComments(), HttpStatus.OK);
    }

    //TODO return updated
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PutMapping(path = "comment/update/{sourceType}/{sourceId}/{commentId}")
    public void updateComment(@PathVariable("sourceType") String sourceType,
                              @PathVariable("sourceId") String sourceId,
                              @PathVariable("commentId") String commentId,
                              @RequestParam(name = "text") String text) {
        service.updateCommentWithTemplate(sourceType, sourceId, commentId, text);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "comment/delete/all")
    public void deleteAllComments() {
        service.deleteAllComments();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @DeleteMapping(path = "comment/delete/{sourceType}/{sourceId}/{commentId}")
    public void deleteComment(@PathVariable("sourceType") String sourceType,
                              @PathVariable("sourceId") String sourceId,
                              @PathVariable("commentId") String commentId) {
        service.deleteComment(sourceType, sourceId, commentId);
    }
}
