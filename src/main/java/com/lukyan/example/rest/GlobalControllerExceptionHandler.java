package com.lukyan.example.rest;

import com.lukyan.example.dto.ErrorResponse;
import com.lukyan.example.exception.NotFoundException;
import com.lukyan.example.helper.RestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
class GlobalControllerExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(value = {NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse notFoundException(NotFoundException ex) {
        log.error(ex.getMessage(), ex);
        return RestHelper.instance().
                errorResponse(ex);
    }

    @ExceptionHandler(value = {Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse throwable(Throwable ex) {
        log.error(ex.getMessage(), ex);
        return RestHelper.instance().errorResponse(ex);
    }

}