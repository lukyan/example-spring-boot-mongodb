package com.lukyan.example.rest;

import com.lukyan.example.constant.Admins;
import com.lukyan.example.constant.Roles;
import com.lukyan.example.constant.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import java.util.Collection;
import java.util.Set;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CommentSecurity extends WebSecurityConfigurerAdapter {

    private static final Collection<String> USERS = Set.of(
            Users.USER1,
            Users.USER2,
            Users.USER3,
            Users.USER4,
            Users.USER5
    );

    private static final Collection<String> ADMINS = Set.of(
            Admins.ADMIN1
    );

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests().anyRequest().authenticated()
                .and().httpBasic();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        var configurer = auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance());
        USERS.forEach(user -> addUser(configurer, user, false));
        ADMINS.forEach(admin -> addUser(configurer, admin, true));
    }

    private void addUser(UserDetailsManagerConfigurer configurer, String user, boolean isAdmin) {
        String role = isAdmin ? Roles.ROLE_ADMIN : Roles.ROLE_USER;
        configurer.withUser(user).password("password").authorities(role);
    }
}
