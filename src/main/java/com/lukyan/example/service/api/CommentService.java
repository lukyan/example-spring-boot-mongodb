package com.lukyan.example.service.api;

import com.lukyan.example.dto.CommentThreadDto;
import com.lukyan.example.mongo.entity.Comment;
import com.lukyan.example.mongo.entity.CommentSourceType;
import com.lukyan.example.mongo.entity.CommentThread;
import com.lukyan.example.mongo.entity.ThreadSourceType;

import java.util.List;

public interface CommentService {
    CommentThread createThread(ThreadSourceType sourceType, String sourceId);

    CommentThreadDto getThread(ThreadSourceType sourceType, String sourceId, String id);

    Comment addComment(CommentSourceType sourceType, String sourceId, String author, String text);

    Comment updateComment(String sourceType, String sourceId, String commentId, String text);

    long updateCommentWithTemplate(String sourceType, String sourceId, String commentId, String text);

    void deleteComment(String sourceType, String sourceId, String commentId);

    List<Comment> getAllComments();

    void deleteAllComments();
}
