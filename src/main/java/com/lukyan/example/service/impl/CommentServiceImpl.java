package com.lukyan.example.service.impl;

import com.lukyan.example.dto.CommentThreadDto;
import com.lukyan.example.exception.NotFoundException;
import com.lukyan.example.mongo.entity.Comment;
import com.lukyan.example.mongo.entity.CommentSourceType;
import com.lukyan.example.mongo.entity.CommentThread;
import com.lukyan.example.mongo.entity.ThreadSourceType;
import com.lukyan.example.mongo.repository.CommentRepository;
import com.lukyan.example.mongo.repository.CommentThreadRepository;
import com.lukyan.example.service.api.CommentService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentThreadRepository commentThreadRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private MongoOperations mongoTemplate;

    @Override
    public CommentThread createThread(@NonNull ThreadSourceType sourceType, @NonNull String sourceId) {
        var now = Instant.now();
        var commentThread =
                new CommentThread(UUID.randomUUID().toString(), sourceType, sourceId, now, now);
        return commentThreadRepository.save(commentThread);
    }

    private CommentThreadDto extractThread(CommentThread commentThread) {
        if (commentThread == null) {
            throw new NotFoundException("Comment not found");
        }
        var sourceType = commentThread.getSourceType();
        var sourceId = commentThread.getSourceId();
        Set<String> authors = new HashSet<>();
        List<Comment> comments = new ArrayList<>();

        var commentEntities = commentRepository.findBySourceId(commentThread.getSourceId());
        commentEntities.forEach(commentEntity -> {
            authors.add(commentEntity.getAuthor());
            comments.add(commentEntity);
            comments.addAll(extractComments(commentEntity));
        });
        return new CommentThreadDto(sourceType, sourceId, authors, comments);
    }

    private List<Comment> extractComments(@NonNull Comment comment) {
        List<Comment> result = new ArrayList<>();
        List<Comment> comments = commentRepository.findBySourceId(comment.getSourceId());
        comments.forEach(entity -> {
            result.add(entity);
            result.addAll(extractComments(entity));
        });
        return result;
    }

    @Override
    public CommentThreadDto getThread(@NonNull ThreadSourceType sourceType, @NonNull String sourceId, @NonNull String id) {
        var commentThread = commentThreadRepository.
                findBySourceTypeAndSourceIdAndId(sourceType, sourceId, id);
        if (commentThread.isEmpty()) {
            throw new NotFoundException(String.
                    format("Thread with sourceType %s and sourceId %s and id %s not found", sourceType, sourceId, id));
        }
        return extractThread(commentThread.get());
    }

    @Override
    public Comment addComment(@NonNull CommentSourceType sourceType, @NonNull String sourceId,
                              @NonNull String author, @NonNull String text) {
        var commentThread = commentThreadRepository.findById(sourceId);
        if (commentThread.isEmpty()) {
            throw new NotFoundException(String.format("Thread with id %s not found", sourceId));
        }
        var now = Instant.now();
        var commentEntity = new Comment(UUID.randomUUID().toString(),
                sourceType, sourceId, author, now, now, text);
        return commentRepository.save(commentEntity);
    }

    @Override
    @Transactional
    public long updateCommentWithTemplate(@NonNull String sourceType, @NonNull String sourceId,
                                          @NonNull String commentId, @NonNull String text) {
        return mongoTemplate.upsert((new Query()).addCriteria(Criteria.where("sourceType").is(sourceType)
                        .andOperator(Criteria.where("sourceId").is(sourceId)
                                .andOperator(Criteria.where("id").is(commentId)))),
                (new Update()).set("text", text), Comment.class).getModifiedCount();
    }

    @Override
    @Transactional
    public Comment updateComment(@NonNull String sourceType, @NonNull String sourceId,
                                 @NonNull String commentId, @NonNull String text) {
        var optionalCommentEntity = commentRepository.
                findBySourceTypeAndSourceIdAndId(sourceType, sourceId, commentId);
        if (optionalCommentEntity.isPresent()) {
            var now = Instant.now();
            var commentEntity = optionalCommentEntity.get();
            commentEntity.setText(text);
            commentEntity.setUpdatedAt(now);
            return commentRepository.save(commentEntity);
        } else {
            throw new NotFoundException(String.format("Comment with id %s not found", commentId));
        }
    }

    @Override
    public void deleteComment(@NonNull String sourceType, @NonNull String sourceId, @NonNull String commentId) {
        commentRepository.deleteBySourceTypeAndSourceIdAndId(sourceType, sourceId, commentId);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public void deleteAllComments() {
        commentRepository.deleteAll();
    }
}
