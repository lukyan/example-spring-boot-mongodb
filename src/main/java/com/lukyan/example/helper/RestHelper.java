package com.lukyan.example.helper;

import com.lukyan.example.dto.ErrorResponse;

import java.io.PrintWriter;
import java.io.StringWriter;

public class RestHelper {

    private RestHelper() {
    }

    private static class SingletonHolder {
        static final RestHelper INSTANCE = new RestHelper();
    }

    public static RestHelper instance() {
        return SingletonHolder.INSTANCE;
    }

    public ErrorResponse errorResponse(Throwable throwable) {
        String stackTrace = stackTraceToString(throwable);
        return new ErrorResponse(throwable.getMessage(), stackTrace);
    }

    public String stackTraceToString(Throwable e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }
}
