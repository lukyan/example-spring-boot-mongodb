package com.lukyan.example.mongo.repository;

import com.lukyan.example.mongo.entity.CommentThread;
import com.lukyan.example.mongo.entity.ThreadSourceType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RestResource(path = "threads", rel = "threads", description = @Description("CommentThreadRepository path"))
public interface CommentThreadRepository extends MongoRepository<CommentThread, String> {
    Optional<CommentThread> findBySourceTypeAndSourceIdAndId(ThreadSourceType sourceType, String sourceId, String id);
}
