package com.lukyan.example.mongo.repository;

import com.lukyan.example.mongo.entity.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends MongoRepository<Comment, String> {
    List<Comment> findBySourceId(String sourceId);

    void deleteBySourceTypeAndSourceIdAndId(String sourceType, String sourceId, String commentId);

    Optional<Comment> findBySourceTypeAndSourceIdAndId(String sourceType, String sourceId, String commentId);
}
