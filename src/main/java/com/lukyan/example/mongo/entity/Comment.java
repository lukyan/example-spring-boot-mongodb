package com.lukyan.example.mongo.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("comments")
public class Comment {
    @Id
    @ApiModelProperty(notes = "Comment ID")
    private String id;

    @NonNull
    @ApiModelProperty(notes = "Source type thread/comment")
    private CommentSourceType sourceType;

    @NonNull
    @ApiModelProperty(notes = "Source ID")
    private String sourceId;

    @NonNull
    @ApiModelProperty(notes = "Author")
    private String author;

    @NonNull
    @ApiModelProperty(notes = "Date created (without timezone)")
    private Instant createdAt;

    @NonNull
    @ApiModelProperty(notes = "Date updated (without timezone)")
    private Instant updatedAt;

    @NonNull
    @ApiModelProperty(notes = "Text")
    private String text;
}
