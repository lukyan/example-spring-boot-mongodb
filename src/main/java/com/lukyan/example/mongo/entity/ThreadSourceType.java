package com.lukyan.example.mongo.entity;

public enum ThreadSourceType {
    NONE, THREAD, COMMENT;
}
