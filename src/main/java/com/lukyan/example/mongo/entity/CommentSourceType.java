package com.lukyan.example.mongo.entity;

public enum CommentSourceType {
    THREAD, COMMENT;
}
