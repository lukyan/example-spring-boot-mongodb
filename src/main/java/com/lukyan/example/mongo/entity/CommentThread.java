package com.lukyan.example.mongo.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("comment_threads")
public class CommentThread {

    @Id
    @ApiModelProperty(notes = "Thread ID")
    private String id;

    @NonNull
    @ApiModelProperty(notes = "Source type none/thread")
    private ThreadSourceType sourceType;

    @NonNull
    @ApiModelProperty(notes = "Source ID")
    private String sourceId;

    @NonNull
    @ApiModelProperty(notes = "Date created (without timezone)")
    private Instant createdAt;

    @NonNull
    @ApiModelProperty(notes = "Date updated (without timezone)")
    private Instant updatedAt;
}
