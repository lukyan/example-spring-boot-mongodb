package com.lukyan.example.exception;

import com.lukyan.example.dto.ErrorResponse;
import com.lukyan.example.helper.RestHelper;
import lombok.experimental.Delegate;

abstract public class AbstractException extends RuntimeException {

    @Delegate
    private ErrorResponse errorResponse = new ErrorResponse();

    public AbstractException(Throwable throwable) {
        super(throwable);
        setErrorMessage(throwable.getMessage());
        setDeveloperMessage(RestHelper.instance().stackTraceToString(throwable));
    }

    public AbstractException(String errorMessage) {
        super(errorMessage);
        setErrorMessage(errorMessage);
    }

    public AbstractException(String errorMessage, String developerMessage) {
        super(errorMessage);
        setErrorMessage(errorMessage);
        setDeveloperMessage(developerMessage);
    }

    public AbstractException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
        setErrorMessage(errorMessage);
        setDeveloperMessage(RestHelper.instance().stackTraceToString(throwable));
    }
}

