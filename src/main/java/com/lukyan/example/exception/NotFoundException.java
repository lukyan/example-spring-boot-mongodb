package com.lukyan.example.exception;

public class NotFoundException extends AbstractException {
    public NotFoundException(String message) {
        super(message);
    }
}
