package com.lukyan.example.dto;

import com.lukyan.example.mongo.entity.Comment;
import com.lukyan.example.mongo.entity.ThreadSourceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentThreadDto {
    @NonNull private ThreadSourceType sourceType;
    @NonNull private String sourceId;
    @NonNull private Collection<String> authors;
    @NonNull private List<Comment> comments;
}
