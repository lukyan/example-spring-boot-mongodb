package com.lukyan.example.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {

    @Getter
    @Setter
    private String errorMessage;
    @Getter
    @Setter
    private String developerMessage;

    public ErrorResponse() {
    }

    public ErrorResponse(@NonNull String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorResponse(@NonNull String errorMessage, String developerMessage) {
        this.errorMessage = errorMessage;
        this.developerMessage = developerMessage;
    }
}
